# Hello World NGINX

## Build image using Dockerfile 

Usage:  docker build [OPTIONS] PATH | URL | -

Note: you may open the command prompt on the current directory where you saved the Dockerfile so that you can use '.' as the path

```bash
#docker build -t reekoh:1.0 .
```

## Check the image built 
to confirm that the image has been built
```bash
#docker images
```

## Run the image to a container
```bash
#docker run -d --name=reekohtest -p 80:80 reekoh:1.0
```
Note: Your webpage is accessible in http://localhost:80
FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y nginx
RUN mkdir www
RUN mkdir www/data

RUN echo " daemon off; \
events {\
    worker_connections 1024;\
}\
http {\
    server {\
        listen 80;\
        location / {\
            root /www/data;\
        }\
    }\
} " > /etc/nginx/nginx.conf

RUN echo " <!doctype html>\
<html>\
  <head>\
    <title>Hello Reekoh</title>\
    <meta charset="utf-8" />\
  </head>\
  <body>\
    <h1>\
      Hello, world and hello, Reekoh!\
    </h1>\
  </body>\
</html> " > /www/data/index.html


EXPOSE 80
CMD ["nginx"]